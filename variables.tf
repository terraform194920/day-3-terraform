variable "region" {
  type = string
  default = "ap-southeast-1"
}

variable "cidr_block" {
  type = string
  default = "10.0.0.0/16"
}

variable "azs" {
  type = string
  default = "ap-southeast-1a"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ami" {
  type = string
  default = "ami-05b46bc4327cf9d99"
}
